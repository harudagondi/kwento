# kwento

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pipeline status](https://gitlab.com/harudagondi/kwento/badges/master/pipeline.svg)](https://gitlab.com/harudagondi/kwento/commits/master)
TODO: Put more badges here.

A Rust-powered interactive fiction engine.

TODO: Fill out this long description.

## Table of Contents

- [kwento](#kwento)
  - [Table of Contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Install

```zsh
```

## Usage

```rust
```

## API

## Maintainers

[@harudagondi](https://github.com/harudagondi)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

LGPL-3.0 © 2020 Gio Genre De Asis
