//! # Kwento
//! A Rust-powered interactive fiction engine.

use std::error::Error as RustError;
use std::result::Result as RustResult;
use std::fmt;
use std::rc::Rc;

//-//-//-//
// TYPES //
//-//-//-//

// (String, String) is confusing, and I don't want to make a struct just for this
type VerbName = String;
type VerbData = Vec<String>;

/// All specific Errors from `kwento` should come from kwento::Result<T>
type Result<T> = RustResult<T, Error>;

//-//-//-///
// ERRORS //
//-//-//-///

/// ## Error
#[derive(Debug)]
pub enum Error {
    InvalidCommand(String),
    VerbNotFound(VerbName, VerbData),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::InvalidCommand(cmd) => write!(f, "invalid command `{}`", cmd),
            Error::VerbNotFound(n, d) => write!(f, "verb `{}` not found, with data: `{:?}`", n, d)
        }
    }
}

impl RustError for Error {}


//-//-//-//-//-//-//
// ENTITY CLASSES //
//-//-//-//-//-//-//

/// _Note_: I blatantly copied the concept of entity classes from the Alan IF language.
/// Here are the website for Alan: https://www.alanif.se/
/// However, I did not look at the source code itself,
/// I just referred to its documentation, lol

/// ## Entity
/// All entities has a name and description.
pub trait Entity {
    fn name(&self) -> String;
    fn description(&self) -> String;
}

/// ## Location
/// All locations are entities.
/// Aside from a name and description, it also have exits (containing the names of the location).
pub trait Location: Entity {
    fn exits(&self) -> Vec<String>;
}

/// ## Tangible
/// All tangibles are entities.
/// These are entities that the protagonist can interact with.
/// Aside from a name and description, it has its current location, and a vector of Descriptors.
pub trait Tangible: Entity {
    fn current_location(&self) -> String;
    fn descriptors(&self) -> Vec<Rc<dyn Descriptor>>;
}

/// ## Item
/// All items are tangibles.
/// These have no specific functionality right now, only that
/// a container can only contain an item.
pub trait Item {
    //TODO: what should an item have?
    // Also note: A container can only contain items, not a tangible
    // you cannot contain an actor lol
}

/// ## Actor
/// All actors are tangibles.
/// Depending on the Kwento object, give a dialogue based on that.
pub trait Actor {
    fn scripts(&self) -> Vec<Rc<dyn Fn(Kwento) -> String>>;
}

/// ## Protagonist
/// A protagonist is a single instance.
/// Aside from various scripts, it also have an inventory.
pub trait Protagonist {
    fn inventory(&self) -> &mut Rc<dyn Container>;
}

//-//-//-//-//-//-//
// HELPER CLASSES //
//-//-//-//-//-//-//

// because of how this works, we do not rely on generics. However, we must implement monomorphization manually
// In COMMON IMPLEMENTATIONS, we define all primitives as a Literal
// Also, if you don't want to implement a Literal,
// use the `literal!` macro using a class that can be cloned
pub trait Literal {
    fn data(&self) -> Self where Self: Sized;
}

/// ## Descriptor
/// Imagine a hashmap, but instead of `HashMap<K, V>`,
/// it becomes `HashMap<String, Box<dyn Anything>>`
/// without the stupid generics.
pub trait Descriptor {
    fn name(&self) -> String;
    fn data(&self) -> Rc<dyn Literal>;
    fn data_mut(&mut self, new_data: Rc<dyn Literal>);
}

use std::collections::HashMap;

pub trait Container {
    fn items(&self) -> HashMap<String, Rc<dyn Item>>;
    fn receive(&mut self, item: Rc<dyn Item>);
    fn transfer(&mut self, item_name: String, new_container: &mut Rc<dyn Container>);
}

pub enum Command { //FIXME: This should be a trait, but i'm too lazy lol.
    Continue,
    Quit,
}

//-//-//-//-//-/
// PROCESSORS //
//-//-//-//-//-/

/// ## Syntax
pub trait Syntax {
    fn is_match(&self, input: &str) -> bool;
    fn interpret(&self, input: String) -> (VerbName, VerbData);
}

/// ## Verb
pub trait Verb {
    fn name(&self) -> VerbName;
    fn process(&self, kwento: &mut Kwento, input_data: VerbData) -> Command;
    fn syntax(&self) -> Rc<dyn Syntax>;
}

//-//-//-//
// INPUT //
//-//-//-//

pub trait InputEngine {
    fn read_command(&self) -> String;
}

pub struct Interpreter {
    syntaxes: Vec<Rc<dyn Syntax>>,
    input_engine: Rc<dyn InputEngine>,
}

impl Interpreter {
    fn interpret(&self) -> Result<(VerbName, VerbData)> {
        let input = self.input_engine.read_command();
        
        for syntax in self.syntaxes.iter() {
            if syntax.is_match(&input) {
                return Ok(syntax.interpret(input));
            }
        } 
        Err(Error::InvalidCommand(input))
    }
}


//-//-//-///
// OUTPUT //
//-//-//-///
// I don't know what I'm doing with this.

pub trait OutputEngine {
    fn render(&self, kwento: &Kwento);
}

pub struct Observer {
    output_engine: Rc<dyn OutputEngine>,
}

impl Observer {
    fn update(&self, kwento: &Kwento) {
        self.output_engine.render(kwento);
    }
}


//-//-//-///
// KWENTO //
//-//-//-///

/// # Kwento
pub struct Kwento {
    pub actors: Vec<Rc<dyn Actor>>,
    pub locations: Vec<Rc<dyn Location>>,
    pub items: Vec<Rc<dyn Item>>,
    pub verbs: Vec<Rc<dyn Verb>>,
    pub interpreter: Interpreter,
    pub observer: Observer,
    pub protagonist: Rc<dyn Protagonist>,
}

impl Kwento {
    pub fn new(protag: Rc<dyn Protagonist>, inp_eng: Rc<dyn InputEngine>, out_eng: Rc<dyn OutputEngine>) -> Self {
        Self {
            actors: Vec::new(),
            locations: Vec::new(),
            items: Vec::new(),
            verbs: Vec::new(),
            interpreter: Interpreter {
                syntaxes: Vec::new(),
                input_engine: inp_eng,
            },
            observer: Observer {
                output_engine: out_eng,
            },
            protagonist: protag
        }
    }

    pub fn init(&mut self) {
        // Initialize Syntaxes from Verbs
        for v in self.verbs.iter() {
            self.interpreter.syntaxes.push(
                v.syntax()
            )
        }
    }

    pub fn run(&mut self) { //FIXME: When Command becomes a trait, refactor this
        let mut command: Command = Command::Continue;
        loop {
            match command {
                Command::Continue => command = self.process(),
                Command::Quit => break,
            }
        }
    }

    fn process(&mut self) -> Command {
        // Input
        let (verb_name, verb_data) = self.interpreter.interpret().unwrap(); // FIXME: remove unwrap

        // Process
        let verb = self.verbs
            .iter()
            .find(|v| v.name() == verb_name)
            .ok_or(Error::VerbNotFound(verb_name, verb_data.clone()))
            .unwrap() //FIXME: replace unwrap with another alternative that handles Errors
            .clone(); //NOTE: if you remove this, error E0502 will appear. I do not fully understand this 
            // The verbs should be type Vec<Rc<Verb>>, and this seems magical to me
            // So I replaced all Box with Rc
            // Please tell me whether a certain Vec should contain a Box or an Rc. Thx
            // ~harudagondi

        let output = verb.process(self, verb_data);

        // Output
        self.notify();

        // Message
        output
    }

    fn notify(&self) {
        self.observer.update(self);
    }
}

//-//-//-//-//-//-//-//-//-/
// COMMON IMPLEMENTATIONS //
//-//-//-//-//-//-//-//-//-/

/// ### Literal Macro
/// As long as the type itself is clonable, then that type can be a Literal
/// The Literals defined by kwento.rs are the following:
/// - isize, and other variants
/// - usize, and other variants
/// - String
#[macro_export]
macro_rules! literal { 
    ( $( $type:ty ),* ) => {
        $(
        impl Literal for $type {
            fn data(&self) -> Self {
                self.clone()
            }
        } 
        )*
    };
}

// implement common literals
literal!(i128, i64, i32, i16, i8, isize, u128, u64, u32, u16, u8, usize, String);

// TODO: Make these a macro, so i dont have to type all of these
pub struct Quit();

impl Syntax for Quit {
    fn is_match(&self, input: &str) -> bool {
        let i: Vec<&str> = input.split_whitespace().collect();
        i.len() == 1 && i[0].to_lowercase() == "quit"
    }

    fn interpret(&self, _: String) -> (VerbName, VerbData) {
        ("quit".to_string(), Vec::new())
    }
}

impl Verb for Quit {
    fn name(&self) -> String {
        "quit".to_string()
    }

    fn process(&self, _kwento: &mut Kwento, _input_data: VerbData) -> Command {
        Command::Quit //FIXME: When Command is refactored, fix this
    }

    fn syntax(&self) -> Rc<dyn Syntax> {
        Rc::new(Quit())
    }
}




#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn syntax_matches_correctly() {
        let q = Quit();
        
        assert!(q.is_match("quit"));
        assert!(q.is_match("quit "));
        assert!(q.is_match("quit                 "));
        assert!(q.is_match(" quit"));
        assert!(q.is_match(" quit "));
        
        assert!(! q.is_match(""));
    }
}
